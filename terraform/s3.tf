
# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "s3-job-offer-bucket-locqueneux" {
  bucket = "s3-job-offer-bucket-locqueneux-owen"
  force_destroy = true
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_object

resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.s3-job-offer-bucket-locqueneux.bucket
  key    = "job_offers/raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3-job-offer-bucket-locqueneux.bucket

  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda_function.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.lambda_allow_bucket]
}